var ApplicationMain = function() {

    var self = this,
        express = require('express'),
        http = require('http'),
        app = express(),
        fs = require("fs"),
        server = http.createServer(app),
        db = {},
        MongoClient = require('mongodb').MongoClient,
        assert = require('assert'),
        format = require('util').format,
        bodyParser = require('body-parser'),
        moment = require('moment'),
        ObjectID = require('mongodb').ObjectID;

    initialise();

    function setupWebServer() {

        server.listen(8000, '192.168.2.150', function() {
            var host = server.address().address
            var port = server.address().port
            setupDatabase();
            console.log("Application runs at http://%s:%s", host, port)
        })

        // app.use(function(req, res, next) {
        //     res.header("Access-Control-Allow-Origin", "*");
        //     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        //     next();
        // });


        app.use(express.static(__dirname));

        // Put these statements before you define any routes.
        app.use(bodyParser.urlencoded({
            extended: true
        }));

        app.all('/', function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            next();
        });

        //registration Api Call
        app.post('/register', function(req, res) {
            req.setEncoding('utf8');
            req.on('data', function(data) {
                // try {
                //     data = JSON.parse(data);
                // } catch (e) {
                //   res.status(500).json({
                //       error: err
                //   });
                // }
                data = JSON.parse(data);
                if (data) {
                    registerUser(data, function(err, result) {
                        if (err) {
                            res.status(500).json({
                                error: err
                            });
                        } else {
                            var userData = {};
                            userData.id = result[0]._id;
                            userData.userName = result[0].userName;
                            res.send(userData);
                        }
                    });
                } else {
                    res.status(500).json({
                        error: err
                    });
                }
            });

            req.on('error', function(error) {
                console.log(error);
                res.status(500).json({
                    error: error
                });
            });
        });

        //login Api Call
        app.post('/login', function(req, res) {
            req.setEncoding('utf8');
            req.on('data', function(data) {
                data = JSON.parse(data);
                if (data) {
                    getOneData(data, 'users', function(err, user) {
                        if (err) {
                            res.status(500).json({
                                error: 'Login Credentials are not valid'
                            });
                        } else if (user && user.length) {
                            var userData = {};
                            userData.id = user[0]._id;
                            userData.userName = user[0].userName;
                            res.send(userData);
                        } else {
                            res.status(500).json({
                                error: 'Login Credentials are not valid'
                            });
                        }
                    });
                } else {
                    res.status(500).json({
                        error: 'Login Credentials are not valid'
                    });
                }
            });

            req.on('error', function(error) {
                console.log(error);
                res.status(500).json({
                    error: 'Login Credentials are not valid'
                });
            });
        });

        //add Event api call
        app.post('/addEvent', function(req, res) {
            req.setEncoding('utf8');
            req.on('data', function(data) {
                data = JSON.parse(data);
                if (data) {

                    getAllEventsBetweenTwoDates(data, function(e, r) {
                        if (r.length == 0 && !e) {
                            addData(data, function(err, result) {
                                if (err) {
                                    res.status(500).json({
                                        error: 'something is wrong while adding event'
                                    });
                                } else {
                                    res.send(result[0]);
                                }
                            });
                        } else {
                            res.status(500).json({
                                error: 'Some one booked this slot already!'
                            });
                        }
                    });

                } else {
                    res.status(500).json({
                        error: 'something is wrong while adding event'
                    });
                }
            });

            req.on('error', function(error) {
                console.log(error);
                res.status(500).json({
                    error: 'something is wrong while adding event'
                });
            });
        });

        //edit a event
        // Modified by kesavan - 28/04/16 - reason api not working
        app.put('/editEvent', function(req, res) {
            req.setEncoding('utf8');
            req.on('data', function(data) {
                data = JSON.parse(data);
                if (data) {
                    var id = data._id;
                    //delete data._id;
                    updateData(id, 'events', data, function(e, r) {
                        if (r) {
                            res.send(r[0]);
                        } else {
                            res.status(500).json({
                                error: 'something is wrong while editing event'
                            });
                        }
                    });
                } else {
                    res.status(500).json({
                        error: 'something is wrong while editing event'
                    });
                }
            });
        });

        //deleting a Event api call
        app.get('/deleteEvent/:_id', function(req, res) {
            if (req.params && req.params._id) {
                deleteData(req.params, 'events', function(err, result) {
                    if (result && result.deletedCount >= 1) {
                        res.send({
                            success: 'event deleted Successfully'
                        });
                    } else if (err) {
                        res.status(500).json({
                            error: err
                        });
                    } else {
                        res.status(500).json({
                            error: "Event is not available to delete"
                        });
                    }
                });
            } else {
                res.status(500).json({
                    error: 'No event id found'
                });
            }

        });

        //getting any one Event by Id Api Call
        app.get('/getOneEvent/:_id', function(req, res) {
            if (req.params && req.params._id) {
                req.params._id = new ObjectID.createFromHexString(req.params._id);
                getOneData(req.params, 'events', function(err, result) {
                    if (result && result.length) {
                        res.send(result[0]);
                    } else if (err) {
                        res.status(500).json({
                            error: err
                        });
                    } else {
                        res.status(500).json({
                            error: 'Event is not found'
                        });
                    }
                });
            } else {
                res.status(500).json({
                    error: 'No Event id found'
                });
            }

        });



            /* The folowing is used for to search user by email id and with limit using GET method
            *  The request will hold two parameters will  following
            *   http://server/search/email?text=aneesh&limit=10
            *   and the response is array for success
            *  [
            *      {
            *        "_id": "5710e7d752dbdd0c7c218ec5",
            *        "userName": "aneesh@mobinius.com"
            *      }
            *    ]
            *  and the error case response will be object
            *   {
            *      "error": {
            *        "message": "Invalid Data. Search String or limit is empty"
            *      }
            *    }
            * Here limit used to tell server to return only 10 users
            */

            app.get('/search/email', function(req, res) {
              var data = {
                "text": req.query.text,
                "limit": parseInt(req.query.limit)
              };
              searchByEmail(data, function(err, data) {
                    if (data) {
                        res.send(data);
                    } else {
                        res.status(500).json({
                            error: err
                        });
                    }
                });
              });


              app.post('/invite', function(req, res) {
                var data = {
                  "text": req.query.text,
                  "limit": parseInt(req.query.limit)
                };
                searchByEmail(data, function(err, data) {
                      if (data) {
                          res.send(data);
                      } else {
                          res.status(500).json({
                              error: err
                          });
                      }
                  });
                });





          /* end of kesavan code */


        //getting All Events Api Call
        app.get('/getAllEvents', function(req, res) {
            // getAllData(function(err, data) {
            getAllEventsForDate(req, function(err, data) {
                if (data) {
                    res.send(data);
                } else {
                    res.status(500).json({
                        error: err
                    });
                }
            });
        });

        app.post('/addUser', function(req, res) {
            addData(req, function(err, data) {
                res.send(data);
            });
        });
    }

    function setupDatabase() {
        MongoClient.connect('mongodb://127.0.0.1:27017/ConferenceRoomBooking', function(err, database) {
            if (err) {
                console.log(err);
            } else {
                db = database;
                // addData(db);
            }
        });
    }

    function addData(data, cb) {
        var collection = db.collection('events');
        collection.insert(data, function(err, docs) {
            getOneData(data, 'events', function(err, res) {
                if (res) {
                    cb(null, res);
                } else {
                    cb(err, null);
                }
            });
        });
    }

    function registerUser(data, cb) {
        var collection = db.collection('users');
        var obj = {};
        obj.userName = data.userName;
        getOneData(obj, 'users', function(error, result) {
            if (result && result.length || error) {
                return cb({
                    err: 'userName already exist'
                }, null);
            } else {
                collection.insert(data, function(err, docs) {
                    getOneData(data, 'users', function(err, res) {
                        if (res) {
                            return cb(null, res);
                        } else {
                            return cb(err, null);
                        }
                    });
                });
            }
        });
    }


    function updateData(id, collectionName, data, cb) {
        var newObjectId = new ObjectID.createFromHexString(id)
        db.collection(collectionName).update({
            "_id": newObjectId
        }, {
            $set: {
                'from': data.from,
                'to': data.to,
                'conferenceRoomNo': data.conferenceRoomNo,
                'eventName': data.eventName
            }
        }, function(err, results) {
            getOneData(newObjectId, collectionName, function(err, res) {
                if (res) {
                    cb(null, res);
                } else {
                    cb(err, null);
                }
            });
        });
    }

    function updateUser(id, collectionName, data, cb) {
        var newObjectId = new ObjectID.createFromHexString(id)
        db.collection(collectionName).updateOne({
            "_id": newObjectId
        }, {
            $set: {
                "name": data.name,
                "place": data.place
            }
        }, function(err, results) {
            getOneData(data, collectionName, function(err, res) {
                if (res) {
                    cb(null, res);
                } else {
                    cb(err, null);
                }
            });
        });
    }

    function deleteData(data, collectionName, cb) {
        var newObjectId = new ObjectID.createFromHexString(data._id);
        db.collection(collectionName).deleteMany({
                "_id": newObjectId
            },
            function(err, results) {
                if (err) {
                    return cb(err, null);
                }
                return cb(null, results);
            }
        );
    }

    function getOneData(data, collectionName, cb) {
        var collection = db.collection(collectionName).find(data);
        collection.toArray(function(err, doc) {
            if (doc) {
                return cb(null, doc);
            } else {
                return cb(err, null);
            }
        });
    }

    //getting all events
    function getAllData(cb) {
        var collection = db.collection('events').find();
        collection.toArray(function(err, doc) {
            if (doc) {
                return cb(null, doc);
            }
            return cb(err, null);
        });
    }

    //getting all events only for today and tomorrow
    function getAllEventsBetweenTwoDates(data, cb) {
        // var startDate = new Date();
        // startDate.setHours(0, 0, 0, 0);
        // startDate = startDate.toISOString();
        //
        // var endDate = startDate;
        // endDate.setDate(endDate.getDate() + 1);
        //
        // // var endDate = new Date();
        // endDate.setHours(23, 59, 59, 999);
        // endDate = endDate.toISOString();

        var collection = db.collection('events').find({
            from: {
                $gte: data.from,
                $lt: data.to
            }
        });

        collection.toArray(function(err, doc) {
            if (doc) {
                return cb(null, doc);
            }
            return cb(err, null);
        });
    }

    function getAllEventsForDate(data, cb) {
        if (data.query && data.query.date) {
            var startDate = new Date(data.query.date);
            startDate.setHours(0, 0, 0, 0);
            startDate = startDate.toISOString();

            var endDate = new Date(data.query.date);
            endDate.setHours(23, 59, 59, 999);
            endDate = endDate.toISOString();

            var collection = db.collection('events').find({
                from: {
                    $gte: startDate,
                    $lt: endDate
                }
            });

            collection.toArray(function(err, doc) {
                if (doc) {
                    // for(var i = 0 ; i < doc.length ; i++) {
                    //    doc[i].from = moment(doc[i].from).format("DD-MM-YYYY HH:mm");
                    //    doc[i].to = moment(doc[i].to).format("DD-MM-YYYY HH:mm");
                    // }
                    return cb(null, doc);
                }
                return cb(err, null);
            });
        } else {
            return cb({
                err: 'Date is not valid'
            }, null);
        }
    }

    /* The following functions added by kesavan */

    function searchByEmail(data, cb){
        var searchString = (data.text ? data.text : undefined),
            limitTo = data.limit ? data.limit : 0;
        var user_collection = db.collection('users').find({
          "userName":{
              $regex: searchString,
              $options: "g"
          }
        }, {"userName":1}).limit(limitTo);

        user_collection.toArray(function(err, doc) {
            if (doc) {
                return cb(null, doc);
            }
            return cb({"message": "Invalid Data. Search String or limit is empty"}, null);
        });
    }

    /* end of kesavan code */



    function initialise() {
        // preInitializer();
        // self.startup();
        setupWebServer();
    }

    //functions
    this.initialise = initialise;
    this.addData = addData;
    this.setupWebServer = setupWebServer;
    this.setupDatabase = setupDatabase;
};

var appMain = new ApplicationMain();


// function checkDuplicateUserExist(cb) {
//     var collection = db.collection('users');
//
//     collection.aggregate({
//             $group: {
//                 // Group by fields to match on (a,b)
//                 _id: {
//                     userName: "$userName"
//                     // password: "$password"
//                 },
//
//                 // Count number of matching docs for the group
//                 count: {
//                     $sum: 1
//                 },
//
//                 // Save the _id for matching docs
//                 docs: {
//                     $push: "$_id"
//                 }
//             }
//         },
//
//         // Limit results to duplicates (more than 1 match)
//         {
//             $match: {
//                 count: {
//                     $gt: 1
//                 }
//             }
//         },
//         function(e, r) {
//             if (r) {
//                 if (r[0].count >= 1) {
//                     return cb({err:'UserName is already Exist'}, true);
//                 } else {
//                     return cb(null, false);
//                 }
//             } else {
//                 return cb(e, false);
//             }
//         }
//     );
// }
